import json
import sys

with open(sys.argv[1]) as json_file:
  final_txt = "level	lat	lng	name\n"

  data = json.load(json_file)
  layers = data["layers"]
  for layer in layers:
    features = layer["features"]
    for feature in features:
      if feature["geometry"]["type"] == "Point":
        if feature["properties"].get("_umap_options", None) and feature["properties"]["_umap_options"].get("zoomTo", None):
          final_txt += str(feature["properties"]["_umap_options"]["zoomTo"])
        else:
          final_txt += "13"
        
        final_txt += "\t"
        final_txt += str(feature["geometry"]["coordinates"][1])
        final_txt += "\t"
        final_txt += str(feature["geometry"]["coordinates"][0])
        final_txt += "\t"
        final_txt += feature["properties"]["name"]
        final_txt += "\n"

        with open("points.tsv","w") as final_file:
          final_file.write(final_txt) 
          final_file.close() 
  json_file.close()